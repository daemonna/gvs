package models

//Core of server
type Core struct {
	MAJOR    int    //Core when you make incompatible API changes,
	MINOR    int    //Core when you add functionality in a backwards compatible manner, and
	PATCH    int    //Core when you make backwards compatible bug fixes.
	HASH     string //to verify integrity of software
	Startime string
	Hostname string
	User     string
	Email    string
	Listen   string
}

//GvsCore holds core variables
var GvsCore Core
