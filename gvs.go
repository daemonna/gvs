package main

import (
	"fmt"
	"time"

	"github.com/peterducai/gvs/api"
	"github.com/peterducai/gvs/models"
)

func main() {

	models.GvsCore.MAJOR = 0
	models.GvsCore.MINOR = 0
	models.GvsCore.PATCH = 1
	models.GvsCore.HASH = "b22"
	models.GvsCore.Startime = time.Now().Format(time.RFC850)

	fmt.Printf("GVS %d.%d.%d %s\n", models.GvsCore.MAJOR, models.GvsCore.MINOR, models.GvsCore.PATCH, models.GvsCore.HASH)
	fmt.Println("starting API server")
	api.Serve()
	fmt.Println("hello")
}
