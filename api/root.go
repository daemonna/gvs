package api

import (
	"fmt"
	"net/http"

	"github.com/peterducai/gvs/models"
)

//Root URL
func Root(w http.ResponseWriter, r *http.Request) {

	w.Header().Add("Strict-Transport-Security", "max-age=63072000; includeSubDomains")
	fmt.Fprintf(w, "GVS %d.%d.%d patch %s\nyour source versioning system.\nstarttime: %s",
		models.GvsCore.MAJOR,
		models.GvsCore.MINOR,
		models.GvsCore.PATCH,
		models.GvsCore.HASH,
		models.GvsCore.Startime)
}
